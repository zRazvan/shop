package org.example.config;

import org.example.entities.*;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class HibernateConfiguration {
    private static SessionFactory sessionFactory;// canalul de comunicare - e singleton, folosim o singura conexiune dar mai multe sesiuni
    public static SessionFactory getSessionFactory(){
        if(sessionFactory==null){
            sessionFactory = new Configuration()
                    .configure("hibernate.config.xml")
                    .addAnnotatedClass(Product.class)
                    .addAnnotatedClass(Suppliers.class)
                    .addAnnotatedClass(Branch.class)
                    .addAnnotatedClass(Client.class)
                    .addAnnotatedClass(Employee.class)
                    .addAnnotatedClass(Category.class)
                    .buildSessionFactory();
        }
        return sessionFactory;
    }
}
