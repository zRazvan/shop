package org.example;

import org.example.config.HibernateConfiguration;
import org.example.entities.Branch;
import org.example.entities.Employee;
import org.example.entities.enums.Job;
import org.example.repository.BranchRepository;
import org.example.repository.EmployeeRepository;

public class MainQueries {

    private static BranchRepository branchRepository = new BranchRepository(HibernateConfiguration.getSessionFactory());
private static EmployeeRepository employeeRepository = new EmployeeRepository(HibernateConfiguration.getSessionFactory());
    public static void main(String[] args) {

        populateDb();
     //   System.out.println(branchRepository.getAllBranchesByLocation("Vitan"));
      //  System.out.println(branchRepository.getAllBrnachesByName("Auchan"));

     //   System.out.println(branchRepository.findBtWordInName("auchan"));
        System.out.println(employeeRepository.findEmployeesByJob(Job.MANAGER));
    }
    public static void populateDb(){
        Branch branch1 = new Branch(null, "Auchan Banu Manta", "Banu Manta");
        branchRepository.create(branch1);
        Branch branch2 = new Branch(null, "Auchan Vitan", "Vitan");
        branchRepository.create(branch2);
        Branch branch3 = new Branch(null, "Auchan Pantelimon", "Pantelimon");
        branchRepository.create(branch3);

        Employee employee1 = new Employee(null,"andrei", Job.CASHIER,branch1);
        Employee employee2 = new Employee(null,"catalin", Job.MANAGER,branch1);
        Employee employee3 = new Employee(null,"ionut", Job.MANAGER,branch1);
        employeeRepository.create(employee1);
        employeeRepository.create(employee2);
        employeeRepository.create(employee3);


    }
}
//afisati toti angajati cu functie de manager
//sa folosim parameter in query