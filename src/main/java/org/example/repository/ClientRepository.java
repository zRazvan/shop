package org.example.repository;

import org.example.entities.Branch;
import org.example.entities.Client;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import java.util.List;

public class ClientRepository extends GenericAbstractRepository<Client> {
    public ClientRepository(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    //select t from Client t
    public String getEntityName() {
        return "Client";
    }

    public Class<Client> getEntityClass() {

        return Client.class;
    }
}
