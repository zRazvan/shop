package org.example.repository;

import org.example.entities.Branch;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import java.util.List;

public class BranchRepository extends GenericAbstractRepository<Branch> {
    public BranchRepository(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    //select t from Branch t
    public String getEntityName() {
        return "Branch";
    }

    public Class<Branch> getEntityClass() {

        return Branch.class;
    }

    public List<Branch> getAllBranchesByLocation(String location){
        Session session = sessionFactory.openSession();
       List<Branch> branchListByLocation = session
               .createQuery("select b from Branch b where location like ' " + location + "'", Branch.class)
               .getResultList();
       session.close();
       return branchListByLocation;
    }

    public List<Branch> getAllBrnachesByName(String name){
        Session session = sessionFactory.openSession();
        String hql = "SELECT b from Branch b where name like '%" + name + "%'";
        List<Branch> branchListByName = session.createQuery(hql,Branch.class).getResultList();
        session.close();
        return branchListByName;
    }

    public List<Branch> findBtWordInName(String word){
        Session session = sessionFactory.openSession();
        String hql = "select b from Branch b where b.name like :wordParam";
        List<Branch> branches = session.createQuery(hql, Branch.class).setParameter("wordParam", "%" + word + "%").getResultList();
        session.close();
        return branches;
    }
}
//afisati toate Branch urile care contin in denumire cuvantul "Auchan"
//sql select * from branch where name like '%auchan%'