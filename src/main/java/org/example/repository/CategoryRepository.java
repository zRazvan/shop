package org.example.repository;

import org.example.entities.Category;
import org.example.entities.Client;
import org.hibernate.SessionFactory;

public class CategoryRepository extends  GenericAbstractRepository{

    public CategoryRepository(SessionFactory sessionFactory) {
        super(sessionFactory);
    }
    public String getEntityName() {

        return "Category";
    }

    public Class<Category> getEntityClass() {

        return Category.class;
    }
}
