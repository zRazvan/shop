package org.example.repository;

import org.example.entities.Branch;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import java.util.List;

public abstract class GenericAbstractRepository<T> {
    protected SessionFactory sessionFactory;

    public GenericAbstractRepository(SessionFactory sessionFactory){
        this.sessionFactory = sessionFactory;
    }

    public T create (T t){
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        session.persist(t);
        transaction.commit();
        session.close();
        return t;
    }

    public T readBy(Integer id){
        Session session = sessionFactory.openSession();
        T t = session.find(getEntityClass(), id);
        session.close();
        return t;

    }

    public List<T> readAll(){
        Session session = sessionFactory.openSession();
        //select t from Branch t   <- pentru BranchRepository
        List<T> tList = session.createQuery("select t from " + getEntityName() + " t ", getEntityClass()).getResultList();/// cod e HQL si nu SQl ce e in " .. "
        session.close();
        return (tList);
    }

    public T updateDetails(T t){
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        t = session.merge(t);
        session.close();
        return t;
    }

    public void delete(T t){
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        session.remove(t);
        transaction.commit();
        session.close();
    }

    public List<T> readAllWithPrintInsideSession() {
        Session session = sessionFactory.openSession();
        // select t from Branch t           <- pentru BranchRepository
        List<T> tList = session.createQuery(
                        "select t from " + getEntityName() + " t",
                        getEntityClass())
                .getResultList();
        System.out.println("From within session");
        System.out.println(tList);
        session.close();
        return tList;
    }

    public abstract String getEntityName();

    public abstract Class<T> getEntityClass();
}
