package org.example.repository;

import org.example.entities.Branch;
import org.example.entities.Suppliers;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import java.util.List;

public class SuppliersRepository extends GenericAbstractRepository<Suppliers> {

    public SuppliersRepository(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    //select t from Suppliers t
    public String getEntityName() {
        return "Suppliers";
    }

    public Class<Suppliers> getEntityClass() {
        return Suppliers.class;
    }


}
