package org.example.repository;


import org.example.entities.Branch;
import org.example.entities.Employee;
import org.example.entities.enums.Job;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import java.util.List;

public class EmployeeRepository extends GenericAbstractRepository<Employee> {
    public EmployeeRepository(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    //select t from Branch t
    public String getEntityName() {
        return "Employee";
    }

    public Class<Employee> getEntityClass() {

        return Employee.class;
    }
//cautam dupa Enum
    public List<Employee> findEmployeesByJob(Job word){
        Session session = sessionFactory.openSession();
        String hql = "select b from Employee b where b.job like :wordParam";
        List<Employee> employees = session.createQuery(hql, Employee.class).setParameter("wordParam", word).getResultList();
        session.close();
        return employees;
    }
}


