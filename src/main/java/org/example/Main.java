package org.example;


import org.example.config.HibernateConfiguration;
import org.example.entities.*;
import org.example.entities.enums.Job;
import org.example.repository.*;
import org.hibernate.SessionFactory;

import java.time.LocalDate;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        System.out.println("Trying to open SessionFactory");
        SessionFactory sessionFactory = HibernateConfiguration.getSessionFactory();
        System.out.println("Session created");

        BranchRepository branchRepository = new BranchRepository(sessionFactory);
        Branch branch1 = new Branch(null, "Mega Image", "Titulescu");
        branchRepository.create(branch1);

        Branch branch2 = new Branch(null, "Auchan", "Banu Manta");
        branchRepository.create(branch2);

        Branch branch3 = new Branch(null, "La Fourmi", "Camil Resu");
        branchRepository.create(branch3);

        Branch readBranch = branchRepository.readBy(2);
        System.out.println(readBranch);

        List<Branch> readAllBranches = branchRepository.readAll();
        System.out.println(readAllBranches);

        branch2.setName("Auchan self service");
        branchRepository.updateDetails(branch2);

        branchRepository.delete(branch1);
        System.out.println(branchRepository.readAll());




        ClientRepository clientRepository = new ClientRepository(sessionFactory);
        Client client1 = new Client(null,"client 1","adresa mail cl1", LocalDate.of(1993,01,30));
        Client client2 = new Client(null,"client 2","adresa mail cl2", LocalDate.of(1994,02,20));
        Client client3 = new Client(null,"client 3","adresa mail cl3", LocalDate.of(1995,03,28));
        Client client4 = new Client(null,"client 4","adresa mail cl4", LocalDate.of(1996,04,27));

        clientRepository.create(client1);
        clientRepository.create(client2);
        clientRepository.create(client3);
        clientRepository.create(client4);

        Client readClient = clientRepository.readBy(2);
        System.out.println(readClient);

        client2.setName("nume client schimbat");
        clientRepository.updateDetails(client2);

        clientRepository.delete(client1);
        System.out.println(clientRepository.readAll());

















    }
}