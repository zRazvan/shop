package org.example;

import org.example.config.HibernateConfiguration;
import org.example.entities.Branch;
import org.example.entities.Employee;
import org.example.entities.Product;
import org.example.entities.Suppliers;
import org.example.entities.enums.Job;
import org.example.repository.BranchRepository;
import org.example.repository.EmployeeRepository;
import org.example.repository.ProductRepository;
import org.example.repository.SuppliersRepository;
import org.hibernate.SessionFactory;

import java.util.List;

public class MainManyToOne {
    public static void main(String[] args) {


        BranchRepository branchRepository = new BranchRepository(HibernateConfiguration.getSessionFactory());
        EmployeeRepository employeeRepository = new EmployeeRepository(HibernateConfiguration.getSessionFactory());


        Branch branch1 = new Branch(null,"avon", "bucuresti");
        Branch branch2 = new Branch(null,"kaufland", "cluj");
        branchRepository.create(branch1);
        branchRepository.create(branch2);


        Employee employee1 = new Employee(null, "nume 1", Job.CASHIER, branch1);
        Employee employee2 = new Employee(null, "nume 2", Job.MANAGER,branch2);
        Employee employee3 = new Employee(null, "nume 3", Job.OTHER,branch2);
        Employee employee4 = new Employee(null, "nume 4", Job.CASHIER, branch2);
        employeeRepository.create(employee1);
        employeeRepository.create(employee2);
        employeeRepository.create(employee3);
        employeeRepository.create(employee4);


        // 1.afisati toti employees (nume,job,branch)
        //2.similar cu ce am facut pentru employee - branch
        //implementati pentru product - supplier  ManyToOne

        List<Employee> readAllEmployees = employeeRepository.readAll();
        System.out.println(readAllEmployees);


        SuppliersRepository suppliersRepository = new SuppliersRepository(HibernateConfiguration.getSessionFactory());
        ProductRepository productRepository = new ProductRepository(HibernateConfiguration.getSessionFactory());

        Suppliers suppliers1 = new Suppliers(null,"supplier1", "Bucuresti");
        Suppliers suppliers2 = new Suppliers(null,"supplier2", "Cluj");
        Suppliers suppliers3 = new Suppliers(null,"supplier3", "Cluj");
        suppliersRepository.create(suppliers1);
        suppliersRepository.create(suppliers2);
        suppliersRepository.create(suppliers3);


        Product product1 = new Product(null,"produs1", "brand1", 10.10, 10.00, 15,suppliers1,null);
        Product product2 = new Product(null,"produs2", "brand2", 20.10, 20.00, 16,suppliers2,null);
        Product product3 = new Product(null,"produs3", "brand3", 30.10, 30.00, 17,suppliers3,null);
        productRepository.create(product1);
        productRepository.create(product2);
        productRepository.create(product3);




    }
}
