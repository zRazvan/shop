package org.example;

import org.example.config.HibernateConfiguration;
import org.example.entities.Category;
import org.example.entities.Product;
import org.example.entities.Suppliers;
import org.example.repository.CategoryRepository;
import org.example.repository.ProductRepository;
import org.example.repository.SuppliersRepository;

import java.util.List;

public class MainLazyLoading {
    private static CategoryRepository categoryRepository = new CategoryRepository(HibernateConfiguration.getSessionFactory());
    private static SuppliersRepository suppliersRepository = new SuppliersRepository(HibernateConfiguration.getSessionFactory());
    private static ProductRepository productRepository =new ProductRepository(HibernateConfiguration.getSessionFactory());
    public static void main(String[] args) {

        populateDB();
        productRepository.readAllWithPrintInsideSession();
        System.out.println("In main");
        System.out.println(productRepository.readAllWithCategories());
    }

    private static void populateDB(){
        CategoryRepository categoryRepository = new CategoryRepository(HibernateConfiguration.getSessionFactory());

        Category category1 = new Category(null, "bio", false);
        Category category2 = new Category(null, "eco", false);
        Category category3 = new Category(null, "alcohol drinks", true);

        categoryRepository.create(category1);
        categoryRepository.create(category2);
        categoryRepository.create(category3);

        SuppliersRepository suppliersRepository = new SuppliersRepository(HibernateConfiguration.getSessionFactory());
        Suppliers suppliers1 = new Suppliers(null,"supplier1", "Bucuresti");
        Suppliers suppliers2 = new Suppliers(null,"supplier2", "Cluj");
        Suppliers suppliers3 = new Suppliers(null,"supplier3", "Cluj");
        suppliersRepository.create(suppliers1);
        suppliersRepository.create(suppliers2);
        suppliersRepository.create(suppliers3);

        ProductRepository productRepository =new ProductRepository(HibernateConfiguration.getSessionFactory());
        Product product1 = new Product(null,"lapte bio","napolact",11.11,5.65,100,suppliers1, List.of(category1));
        Product product2 = new Product(null,"oua","gaina",12.11,6.65,100,suppliers2,List.of(category2));
        Product product3 = new Product(null,"carne","vaca",13.11,7.65,100,suppliers3,List.of(category3));
        Product product4 = new Product(null,"avocado","avocado",14.11,8.65,100,suppliers1,List.of(category2, category1));

        productRepository.create(product1);
        productRepository.create(product2);
        productRepository.create(product3);
        productRepository.create(product4);
        System.out.println(productRepository.readAllWithCategories());
        System.out.println(productRepository.getAllProductsByPrice(15.10));


    }
    }

