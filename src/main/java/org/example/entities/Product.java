package org.example.entities;

import jakarta.persistence.*;

import java.util.List;
//by default cand citesc un produs din baza de date vreau sa nu fie incarcate si categoriile(Lazy fetch)
//asa ca pentru cazul in care vreau sa fie incarcate si categoriile fiecarui produs o sa fac o metoda separata

@Entity
public class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    @Column(nullable = false)
    private String name;
    private String brand;
    private Double price;
    @Column(name = "value_price")
    private Double valuePrice;
    private Integer quantity;
    @ManyToOne

    private Suppliers suppliers;

   // @ManyToMany(fetch = FetchType.EAGER)   eager = aduce o categiredin baza de date de fiecare data cand aduce un produs
    @ManyToMany
    private List<Category> categoriesList;



    public Product(Integer id,String name, String brand, Double price, Double valuePrice, Integer quantity, Suppliers suppliers, List<Category> categoriesList){
    this.id=id;
        this.name = name;
        this.brand = brand;
        this.price = price;
        this.valuePrice = valuePrice;
        this.quantity = quantity;
        this.suppliers = suppliers;
        this.categoriesList = categoriesList;
    }

    public Product() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Double getValuePrice() {
        return valuePrice;
    }

    public void setValuePrice(Double valuePrice) {
        this.valuePrice = valuePrice;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "Product{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", brand='" + brand + '\'' +
                ", price=" + price +
                ", valuePrice=" + valuePrice +
                ", quantity=" + quantity +
                ", suppliers=" + suppliers +
                ", categoriesList=" + categoriesList +
                "\n'}'";
    }
}

